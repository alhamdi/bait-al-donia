﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.Master" AutoEventWireup="true" CodeBehind="Admin-employee.aspx.cs" Inherits="Bauit_aldonia.Admin_employee" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>add new employee</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_contentContainer" runat="server">
    <!--Page Title-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <div id="page-title">
        <h1 class="page-header text-overflow">إدارة الموظفين</h1>

        <!--Searchbox-->
        <div class="searchbox">
            <div class="input-group custom-search-form">
                <input type="text" class="form-control" placeholder="Search..">
                <span class="input-group-btn">
                    <button class="text-muted" type="button"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </div>
    </div>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End page title-->

    <!--Breadcrumb-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Library</a></li>
        <li class="active">Data</li>
    </ol>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End breadcrumb-->

    <div id="page-content">

        <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">إضافة موظف جديد</h4>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="control-label">رقم الهاتف</label>
                                <input type="text" id="txtEmplPhone" runat="server" class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="control-label">إسم المستخدم</label>
                                <input type="text" id="txtUserName" runat="server" class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="control-label">إسم الموظف</label>
                                <input type="text" id="txtEmpName" runat="server" class="form-control">
                            </div>
                        </div>
                    </div>
                        <div class="dz-default dz-message">
                            <div class="dz-icon icon-wrap icon-circle icon-wrap-md">
                                <i class="fa fa-cloud-upload fa-3x"></i>
                            </div>
                            <div>
                                <p class="dz-text">Drop files to upload</p>
                                <p class="text-muted">or click to pick manually</p>
                            </div>
                        </div>
                        <div class="fallback">
                            <input name="file" type="file" multiple />
                        </div>
                    </div>

                <div class="modal-footer">
                    <button id="btnCancle" type="button" class="btn btn-default" runat="server" data-dismiss="modal">إلغاء</button>
                    <button id="btnSave" type="button" class="btn btn-primary" runat="server" onserverclick="addNewEmployee">حفظ</button>
                </div>
            </div>
        </div>
    </div>



    </div>

    <div class="row  panel">
            <div class="col-lg-12">
                <div class="col-lg-4">
                    <div class="form-group">
                         <label class="control-label">نوع البحث</label>
                        <asp:DropDownList ID="lstSearchType" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label class="control-label">كلمة البحث</label>
                        <input type="text" id="txtsearch" runat="server" class="form-control">
                    </div>
                </div>
                <div class="col-lg-2">
                    <p>.</p>
                    <button id="btnSearch" class="btn btn-primary" type="submit" runat="server">بحث</button>
                    <a href="#myModal" class="btn btn-primary" data-toggle="modal">إضافة موظف جديد</a>
                </div>
            </div>
        </div>

    <!-- Employee list -->
    <asp:Repeater ID="Repeater_itemList" runat="server">
            <ItemTemplate>
                <div class="col-lg-2">
                    <div class="panel">
                        <div class="panel-body bg-success text-center">

                            <!--Gauge placeholder-->
                            <canvas id="demo-gauge" height="105" class="canvas-responsive"></canvas>

                        </div>
                        <div class="pad-all">
                            <p class="text-2x"><%#Eval("Number")%></p>

                            <hr>
                            <ul class="list-unstyled row text-center">

                                <!--Gauge info-->
                                <li class="col-xs-6">
                                    <span id="demo-gauge-text" class="text-2x">57</span>
                                    <p class="text-uppercase">
                                        <small>% Server Load</small>
                                    </p>
                                </li>
                                <li class="col-xs-6">
                                    <span class="text-2x"><%#Eval("statusName")%></span>
                                </li>
                            </ul>
                            <button id="btnEdit" class="btn btn-primary" type="submit" runat="server">تعديل</button>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
</asp:Content>
