﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace Bauit_aldonia
{
    public partial class Admin_item : System.Web.UI.Page
    {
        BaitAlduniaEntities1 db = new BaitAlduniaEntities1();
        
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["userID"] = "Ahmed";
            if (!IsPostBack)
            {
                
            }
            listSearchType();
            ItemsType();
        }

        private void listSearchType()
        {
            var itemType = from s in db.ItemsTypes select s;
            lstSearchType.DataSource = itemType.ToList();
            lstSearchType.DataTextField = "Name";
            lstSearchType.DataValueField = "ID";
            lstSearchType.DataBind();
        }

        private void ItemsType()
        {
            var itemType = from s in db.ItemsTypes select s;
            lstItemsType.DataSource = itemType.ToList();
            lstItemsType.DataTextField = "Name";
            lstItemsType.DataValueField = "ID";
            lstItemsType.DataBind();
        }

        protected void btnSearch_ServerClick(object sender, EventArgs e)
        {
            ListItem(int.Parse(lstSearchType.SelectedValue), int.Parse(txtsearch.Value));
        }

        private void ListItem(int _searchType, int _searchWord )
        {

            var m = from i in db.Items
                    join ii in db.ItemsStatus on i.statusID equals ii.ID
                    where (_searchType  == i.TypeID && _searchWord == i.Number)
                    select new { statusName = ii.Name, i.TypeID,i.Number,i.name,i.statusID};
           
            Repeater_itemList.DataSource = m.ToList();
            Repeater_itemList.DataBind();
        }

        protected void AddNewItems(object sender, EventArgs e)
        {
            Item itemObj = new Item();

            var s = (from i in db.ItemsStatus
                    where i.indecator == true
                    select new {i.ID }).FirstOrDefault();

            itemObj.name = txtItemName.Value;
            itemObj.Number = int.Parse(txtItemNum.Value);
            itemObj.TypeID = int.Parse(lstItemsType.SelectedValue);
            itemObj.CreatedOn = DateTime.Now;
            itemObj.CreatedBy = Session["userID"].ToString();
            itemObj.statusID = s.ID;
            db.Items.Add(itemObj);
            db.SaveChanges();
            
        }

        protected void EditItems(object sender, EventArgs e)
        {
            Item itemObj = new Item();

            Button button = (sender as Button);
            int index = int.Parse(button.CommandArgument);

             itemObj = (from i in db.Items
                    where i.ID == index
                    select i).FirstOrDefault();

            //txtItemName.Value = itemObj.name;
            //lstItemStatus.SelectedValue = itemObj.statusID;
            //lstItemType.SelectedValue = itemObj.TypeID;
            //txtItemNum.Value = itemObj.Number;

            itemObj.name = txtItemName.Value;
            itemObj.statusID = int.Parse(lstItemStatus.SelectedValue);
            itemObj.TypeID =int.Parse(lstItemsType.SelectedValue);

            db.SaveChanges();

        }
    }
}