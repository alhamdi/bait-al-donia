﻿<%@ Page Title="" Language="C#" MasterPageFile="~/main.Master" AutoEventWireup="true" CodeBehind="Admin-item.aspx.cs" Inherits="Bauit_aldonia.Admin_item" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>add new item</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder_contentContainer" runat="server">
    <!--Page Title-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <div id="page-title">
        <h1 class="page-header text-overflow">إضافة عناصر جديدة</h1>

        <!--Searchbox-->
        <div class="searchbox">
            <div class="input-group custom-search-form">
                <input type="text" class="form-control" placeholder="Search..">
                <span class="input-group-btn">
                    <button class="text-muted" type="button"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </div>
    </div>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End page title-->


    <!--Breadcrumb-->
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Library</a></li>
        <li class="active">Data</li>
    </ol>
    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
    <!--End breadcrumb-->


   

    <!--Page content-->
    <!--===================================================-->
    <div id="page-content">

        <!-- Modal HTML -->
    <div id="myModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">إضافة مستلزم جديد</h4>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="control-label">نوع المستلزم</label>
                                <asp:DropDownList ID="lstItemsType" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="control-label">رقم المستلزم</label>
                                <input type="text" id="txtItemNum" runat="server" class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label class="control-label">إسم المستلزم</label>
                                <input type="text" id="txtItemName" runat="server" class="form-control">
                            </div>
                        </div>
                    </div>
                        <div class="dz-default dz-message">
                            <div class="dz-icon icon-wrap icon-circle icon-wrap-md">
                                <i class="fa fa-cloud-upload fa-3x"></i>
                            </div>
                            <div>
                                <p class="dz-text">Drop files to upload</p>
                                <p class="text-muted">or click to pick manually</p>
                            </div>
                        </div>
                        <div class="fallback">
                            <input name="file" type="file" multiple />
                        </div>
                    </div>

                <div class="modal-footer">
                    <button id="btnCancle" type="button" class="btn btn-default" runat="server" data-dismiss="modal">إلغاء</button>
                    <button id="btnSave" type="button" class="btn btn-primary" runat="server" onserverclick="AddNewItems">حفظ</button>
                </div>
            </div>
        </div>
    </div>

    <div id="EditModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">تعديل بيانات مستلزم </h4>
                </div>
                <div class="modal-body">
                    <div class="col-lg-12">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label class="control-label">نوع المستلزم</label>
                                <asp:DropDownList ID="lstItemType" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label class="control-label">رقم المستلزم</label>
                                <input type="text" id="Text1" runat="server" class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label class="control-label">إسم المستلزم</label>
                                <input type="text" id="Text2" runat="server" class="form-control">
                            </div>
                        </div>
						<div class="col-lg-3">
                            <div class="form-group">
                                <label class="control-label">حالة المستلزم</label>
                                <asp:DropDownList ID="lstItemStatus" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                        <div class="dz-default dz-message">
                            <div class="dz-icon icon-wrap icon-circle icon-wrap-md">
                                <i class="fa fa-cloud-upload fa-3x"></i>
                            </div>
                            <div>
                                <p class="dz-text">Drop files to upload</p>
                                <p class="text-muted">or click to pick manually</p>
                            </div>
                        </div>
                        <div class="fallback">
                            <input name="file" type="file" multiple />
                        </div>
                    </div>

                <div class="modal-footer">
                    <button id="btnCancleChange" type="button" class="btn btn-default" runat="server" data-dismiss="modal">إلغاء</button>
                    <asp:Button ID="btnSaveChanges" runat="server" Text="حفظ التعديلات" OnClick="EditItems"/>
                </div>
            </div>
        </div>
    </div>


        <div class="row  panel">
            <div class="col-lg-12">
                <div class="col-lg-4">
                    <div class="form-group">
                         <label class="control-label">type of search</label>
                        <asp:DropDownList ID="lstSearchType" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group">
                        <label class="control-label">text</label>
                        <input type="text" id="txtsearch" runat="server" class="form-control">
                    </div>
                </div>
                <div class="col-lg-2">
                    <p>.</p>
                    <button id="btnSearch" class="btn btn-primary" type="submit" runat="server" onserverclick="btnSearch_ServerClick">بحث</button>
                    <a href="#myModal" class="btn btn-primary" data-toggle="modal">إضافة مستلزم جديد</a>
                </div>
            </div>
        </div>

        <!-- item list -->
        <asp:Repeater ID="Repeater_itemList" runat="server">
            <ItemTemplate>
                <div class="col-lg-2">
                    <div class="panel">
                        <div class="panel-body bg-success text-center">

                            <!--Gauge placeholder-->
                            <canvas id="demo-gauge" height="105" class="canvas-responsive"></canvas>

                        </div>
                        <div class="pad-all">
                            <p class="text-2x"><%#Eval("Number")%></p>

                            <hr>
                            <ul class="list-unstyled row text-center">

                                <!--Gauge info-->
                                <li class="col-xs-6">
                                    <span id="demo-gauge-text" class="text-2x">57</span>
                                    <p class="text-uppercase">
                                        <small>% Server Load</small>
                                    </p>
                                </li>
                                <li class="col-xs-6">
                                    <span class="text-2x"><%#Eval("statusName")%></span>
                                </li>
                            </ul>  
                            <%--<a href="#EditModal" class="btn btn-primary" data-toggle="modal" CommandArgument= '<%# Eval("ID") %>'>تعديل</a>--%>
                            <asp:LinkButton runat="server" id="btnUpdate"  CssClass="btn btn-primary" CommandArgument='<%# Eval("ID") %>'
                                                        data-toggle="modal" data-target="#EditModal">تعديل</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>




    </div>
    <!--===================================================-->
    <!--End page content-->
</asp:Content>
